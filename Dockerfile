
FROM openjdk:11 AS  build
WORKDIR /app
COPY . .
RUN ./gradlew build --no-daemon

FROM openjdk:11-jre-slim
EXPOSE 8080
WORKDIR /app
COPY --from=build /app/build/libs/*.jar /app/serving-web-content-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "serving-web-content-0.0.1-SNAPSHOT.jar"]
